package conway;
import java.util.ArrayList;

public class ConwayGrid {

    private ArrayList<String> grid;

    public ConwayGrid() {}

    public ConwayGrid(ArrayList<String> grid) {
        this.grid = grid;
    }

    public ArrayList<String> getGrid() {
        return grid;
    }
    public void setGrid(ArrayList<String> grid) {
        this.grid = grid;
    }
}
