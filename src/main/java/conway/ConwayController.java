package conway;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConwayController {

    @RequestMapping(value = "/nextStep", method=RequestMethod.POST, consumes="application/json")
    public @ResponseBody ConwayGrid conway(@RequestBody ConwayGrid grid) {
        ConwaySolver solver = new ConwaySolver(grid);
        return solver.solve();
    }

}
