package conway;

import java.util.ArrayList;

public class ConwaySolver {
    private ArrayList<String> grid;
    private int height;
    private int width;

    public ConwaySolver(ConwayGrid grid) {
        this.grid = grid.getGrid();        
        this.height = this.grid.size();
        this.width = this.grid.get(0).length();
    }

    public ConwayGrid solve() {
        ArrayList<String> newGrid = new ArrayList<String>();
        
        for (int i = 0; i < height; ++i) {
            String line = "";
            for (int j = 0; j < width; ++j) {
                line += newCell(i, j);
            }
            newGrid.add(line);
        }

        return new ConwayGrid(newGrid);
    }

    private String newCell(int x, int y) {
        String result = "0";
        int liveCount = liveNeighbors(x, y);
        if (isLive(x, y)) {
            if (liveCount == 2 || liveCount == 3) {
                result = "1";
            }
        } else {
            if (liveCount == 3) {
                result = "1";
            }
        }
        return result;
    }

    private int liveNeighbors(int x, int y) {
        int liveCount = 0;
        for (int newX = x - 1; newX <= x + 1; ++newX) {
            for (int newY = y - 1; newY <= y + 1; ++newY) {
                if (newX == x && newY == y) {
                    // this is the current cell, thus not a neighbor
                    continue;
                }
                
                if (newX < 0 || newX >= height || newY < 0 || newY >= width) {
                    // neighbor outside
                    continue;
                }

                if (isLive(newX, newY)) {
                    ++liveCount;
                }
            }
        }
        return liveCount;
    }

    private Boolean isLive(int x, int y) {
        String ch =  Character.toString(grid.get(x).charAt(y));
        return ch.equals("1");
    }
}
