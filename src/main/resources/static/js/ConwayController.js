app.controller('ConwayController',
['$scope', '$http', '$timeout', function($scope, $http, $timeout) {

    $scope.colors = grid;

    $scope.updateCell = function(i, j) {
        // could be prettier
        if ($scope.colors[i][j] === '0') {
            $scope.colors[i] = $scope.colors[i].replaceAt(j, '1'); 
        } else {
            $scope.colors[i] = $scope.colors[i].replaceAt(j, '0');
        }
    };
    
    $scope.buttonText = "Start";
    
    $scope.startButtonClicked = function() {
        if ($scope.buttonText == "Start") {
            $scope.buttonText = "Stop";
            $scope.nextStep();
        } else {
            $scope.buttonText = "Start";
        }
    };

    $scope.clear = function() {
        for (var i = 0; i < $scope.colors.length; ++i) {
            var str = '';
            for (var j = 0; j < $scope.colors[0].length; ++j)
                str += '0';
            $scope.colors[i] = str;
        }
    }

    $scope.nextStep = function() {
        $http.post(
            "nextStep", 
            {"grid": $scope.colors}
        )
        .success(function (response) {
            $scope.colors = response.grid;
            $timeout(function() {
                $scope.displayErrorMsg = false;
                if ($scope.buttonText == "Stop") {
                    $scope.nextStep();
                }
            }, 300);
            return response;
        });
    };
}]);

String.prototype.replaceAt=function(index, replacement) {
    return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
}
