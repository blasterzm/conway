package conway;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ConwayControllerTests {

    @Autowired
    private MockMvc mockMvc;


    @Test
    public void nextStepSimpleTest() throws Exception {
        ArrayList<String> g = new ArrayList<String>();
        g.add("00000");
        g.add("00000");
        g.add("01110");
        g.add("00000");
        g.add("00000");	        
        ConwayGrid inputGrid = new ConwayGrid(g);
        
        g = new ArrayList<String>();
        g.add("00000");
        g.add("00100");
        g.add("00100");
        g.add("00100");
        g.add("00000");
        ConwayGrid outputGrid = new ConwayGrid(g);

       	mockMvc.perform(
       	    post("/nextStep")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inputGrid))
        )
            .andExpect(status().isOk())
            .andDo(print())
            .andExpect(jsonPath("$.grid").value(outputGrid.getGrid()));
    }

}
